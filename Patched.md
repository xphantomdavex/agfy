# List of Patched Games and Softwares
### Alphabetical Order
#### Note: This list does not include Every game just the known ones
## Games

- **ALL** IOS Games and Apps
- **MOST** Ubisoft games: The ones we know work are Far Cry 4, Far cry Primal, Far cry 3 (Incl. Blood dragon), AC Black Flag, AC Origins, Watch Dogs 2  ( AC games, Rayman Legends, ETC.)
- 60 ! Seconds ( not patched just needs an update )
- Batman arkham origins
- Escape From Tarkov
- Fallout New Vegas ( not patched just needs an update )
- Hot Lava Multiplayer
- JackBox 1-3
- Journey to the Savage Planet (Multiplayer - Needs update)
- Left For Dead
- NARUTO TO BORUTO: SHINOBI STRIKER ( worked for someone but not for others im lost )
- Naruto Ultimate Ninja Storm 4
- Portal 2 Multiplayer ( Single Player still functional )
- Predator hunting grounds
- Sea Of Thieves
- Secret Neighbor
- Shotgun Farmers (Multiplayer)
- SimCity
- Streamer Life Simulator (Version mismatch - Needs update)
- Squad (Multiplayer)
- Uno
- 7 Days To Die (Multiplayer)

## Softwares
- Voicemod Pro
- Unity Pro 
- Game Fire Pro
- Website Builder (missing keygen)
- Krisp 
- Discord Bot Maker (needs update)
