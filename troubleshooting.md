## Troubleshooting Guide
##### For common issues unrelated to a specific game or software skip to number 6 in the guide
#### Press Ctrl + F On your keyboard and type in the name of the game or software you are having issues with , if nothing comes up there is not a specific fix for that game/software currently, below are a list of troubleshooting options that may solve your problem. If after reading ALL troubleshooting options you are still having issues create a ticket in #create-ticket 

###### Also note if none of the troubleshooting options work and your getting a specific error message try googling that error code + the name of your game !!

### Basic Troubleshooting 
###### If when searching for your game/software no matched appear follow these steps before creating a ticket please

1.**Turn off your anti-virus/windows defender during extraction and while trying to run.**
    
    1.In 'Settings,' select 'Update & Security'
    
    2.Go to 'Windows Security,' Select 'Virus & threat protection'
    
    3.In the sidebar, select 'Virus & threat protection'
    
    4.Click on 'Virus & threat protection settings'
    
    5.Under 'Real-time protection' click the switch so it says 'Off'

    6.Turning all off is reccomended but not always necessary 

    Video Guide: <https://cdn.discordapp.com/attachments/830166060774588466/830174695634108456/WD.mp4>
2.**Redownload and extact with your anti-virus/windows defender disabled.**

3.**Install everything in redist_**
   
    Should be included with your download. If not, it can be downloaded at https://uploadhub.to/0333a8503473412e061e31804150f908/_Redist.zip

4.**Install directx**
    
    Should be included with your download inside the _redist folder ^. If not, it can be downloaded at https://www.microsoft.com/en-us/download/details.aspx?id=35

5.**Allow your game/program to go thru your firewall or disable your firewall.**

    1.Right-click the Windows Start button and select Control Panel.
    
    2.Click Windows Firewall.
    
    3.Click Advanced Settings.
    
    4.Click Inbound Rules, then New Rule.
   
    5.Select Program from the Rule Type window, then click Next. Note: This includes SQLbrowser.exe.
   
    6.Click Browse below This program path to select the program executable file, then click Next.
    
    7.Ensure Allow the connection is selected, then click Next.
   
    8.Select When to apply the rule (Domain, Private, or Public), then click Next.
   
    9.Enter a Name and optional Description, then click Finish.

6.**Can't extract .rar/.zip?**
   
    Right click the .rar/.zip file and press "extract here". Don't double click it. If winwar is not install it can be downloaded at https://www.rarlab.com/download.htm

7.**Can't find .exe?**
   
    Try redownloading/extracting with your anti-virus off (refer to #1 for intructions) or try checking the "bin" folder

8.**Have steam open and raft said's it isn't?**
    
    Delete the file called glu.32dll  in the Raft folder. 
    
9.**Slow downloads?**
    
    Use FDM type ``".fdm"`` in the discord for more info.

10.**Game running slow or crashing often?**
    
    Find out if your computer can run the game here: https://www.systemrequirementslab.com/cyri

11.**Game opens steam or is not detecting steam ?**

- Follow this: https://discord.com/channels/830164808207826994/830166080051740712/830394828381618176

12.**If your setup was an iso ie codex**

    Make sure you have this checked
    https://media.discordapp.net/attachments/758077808852598887/803568412701229096/Screenshot_20210125-224732_Discord.jpg

13.**If FDM tells you 'no resume support' what do you do**

  Start the download again on your browser --> cancel it --> copy the link of the canceled download --> go to the file that says no resume support --> right click --> change URL --> clear previous URL and paste the one you just copied. If done right, it will resume

14.**DLL missing and installing redist and direct x didnt work go to this site:**

    https://www.dll-files.com
    download the missing dll, extract the rar, and put the dll in your game folder
    if the above doesnt work then also put the dll in your system 32 folder

15.**My game is in a different language, what do I do?**

    Close the game and search .ini in the folder and go in the ini files until you find language= or something like that

    Change it to the language you preferred.

    If the language is like ru_RU or something like that look on here for language codes: https://lh.2xlibre.net/locales/

16.**Infinite ads?**
    
    1) Clear your cache and cookies and then restart your browser

    2) Try a different browser like Opera GX https://opr.as/mc69

     
17.**Game redirecting to steam**
  
    1) Download this: https://mega.nz/file/3yhnGCTJ#828CCvBiUB1lN4aa-d-fqpcuzq1OK8q6L93HwU0sTLM
    2) Right click it and press "Extract to SmartSteamEmu143"
    3) Run the SSELauncher.exe
    4) Then click on 'File', and then 'Add Game', and choose the game .exe and double click on it.
    5) Then it will open a page, you see the 'Game App id' is 0, replace 0 with the number that you can get from 
    these following instructions:           
    Get the steam page link for the game of your choice, for example if the game that you want to run with SSELauncher is 
    Terraria, go to Google and search ‘Terraria steam’, or if it’s another game do it as ‘gamename steam’, and then for 
    example if it’s Terraria the link is: https://store.steampowered.com/app/105600/Terraria/ the number between /app/ and 
    /Terraria/ or /gamename/ is the number that you will replace 0 with. In this situation, replace 0 with 105600 for the 
    ‘Game App     id’.This goes on with other games too.

    6) Tick the 'Use x64 launcher' if you have a pc with 64 bit, and then click 'Save'.
    7) You will see the game icon showing there, double click on it and your game will start!
    If it doesn't work, try putting different game .exe that is in the same extracted folder. 

    What to do if it’s not yet fixed?
    Put it in #game-update-request so it’ll be considered in the future.

**If none of these steps work then start a ticket and we will assist soon**

##### Games

**Stranded Deep** " Steam could not be initialized error "
  1. Go to your "Stranded_Deep_data" folder > " Plugins " Folder  
  2. Open "OnlineFix.ini" with notepad
  3. Change the value of "FakeAppId" to "480"
  4. Run the game as administrator with steam open like always
  5. Enjoy ! :D 


**FDM** Username and Password Prompt 

 1. You are pasting the incorrect link, type !fdm into chat to learn how to download with FDM correctly

**Star Wars Fallen Order**
##### Only works on windows builds below 1909
 1. Press the Windows key on your keyboard to display the taskbar if it isn’t visible.
 2. Right-click the Date/Time display on the taskbar and then choose Adjust Date/Time from the shortcut menu.
 3. Click the Change Date and Time button
 4. Change date and time to December 1st 11th or 31st of 2019

**Call Of Duty: Moder Warfare 2 Remaster** 'Out of memory' error (Please be very careful when following these steps your fault if something goes wrong)

 1. press windows key+r and type ms config
 2. go to the boot tab and switch to safe mode specifically the one that says network
 3. reboot computer
 4. start mw2 and press yes on safe mode for the game
 5. go to mw2 graphics settings and set to windowed mode with borders
 6. exit game restart computer into non safe mode windows and start the game
 7. press no on the pop up and switch the game to full screen once the game is up and running

**cod modern warfare 2: Fatal error**

    Add IW4x as a non-steam game first. Next navigate to your library in steam and right click IW4x and click properties. In the launch options     text-line, type “-nosteam” (no quotations) and click save. Should work now

**Prototype 2** Crash on launch

 1. Unplug your keyboard
 2. Launch the game as administrator
 3. Once the game is done launching plug your keyboard back in
 4. Enjoy :D 
 
**In Silence** Fix files

  1. Download the rar with the following link, extract it and drag the files into the games main folder and hit replace: https://uploadhub.co/7c9579105976dc3f65066520daa94138/AGFY-IS_Fix_Repair_Steam.rar 
  
**Doom Eternal** Asks for bethesda account
- Before doing the below, try enter random log-in details and check if it lets you login.
 1. Open the DRM-free DOOMEternalx64vk.exe (SHA256: 65C1939CA88D613BB78D5BDF3D6804BC171FDE708F4F0F0C11DAF3A4AD492B40) in a hex editor of your choice
 2. replace 8 bytes at 0x684329 with the following: b0 01 48 83 c4 20 5b c3
 3. You MUST block the executable in your firewall. This bypasses the need to log into Bethesda.net completely, tickets etc not required ( for steps on how to block with firewall refrence basic troubleshooting at the top )

**Red Dead Redemption** 

 - ⁠Check if u have enough space for installation.

 •    ⁠Rename the folder of setup to something preferably short & not containing any non-english characters or symbols.

 •    ⁠Do not install the game in a folder or drive with non-english characters or symbols too. (Better with short folder name & not in subfolders)
 
 •    ⁠Install the following package (Complete VC Redist): https://www.softpedia.com/get/System/System-Miscellaneous/MultiPack-Visual-C.shtml
 
 •    ⁠Run the installer as admin.
 
 •    ⁠Add the setup & game's folder & also your Windows temp folder to ur Antivirus exclusion.
 
 •    ⁠Install in another HDD or Drive (& Not Drive C).

 •    ⁠Defrag your HDD & try again.https://www.softpedia.com/get/Tweak/System-Tweak/SmartDefrag.shtml
 
 •    ⁠Install the game in Safe Mode. https://support.microsoft.com/en-us/help/12376/windows-10-start-your-pc-in-safe-mode

 •    ⁠Turn off UAC in Windows.

 •    ⁠Install with Windows 7 Compatibility mode.
 
 •    ⁠Increase Windows PAGE FILE (virtual ram) to at least 8gb.

 •    ⁠Use "Disk Cleanup" to delete unwanted files. Or use CCleaner (Cleaner+Registry Fix): https://www.ccleaner.com/

 •    ⁠If you are not admin, make it: https://www.technipages.com/windows-administrator-account-login-screen

 •    ⁠Rehash the setup files, If there is a torrent for it.
 
 •    ⁠For Fitgirl's repacks, If u have 8GB or less RAM, Check the 3GB limit RAM usage of setup.

ANOTHER IMPORTANT THING: Open the folder inside this archive and then overwrite the files from the folder to the original games files and open launcher.exe with windows defender disabled
https://megaup.net/2frbu/Red.Dead.Redemption.Crackfix.V2-EMPRESS+Mr_Goldberg.7z - **This is needed to fix the crashing issue!**

If you cant figure out how to do some of these things, google is there for you!


**Minecraft Windows 10 edition/ Bedrock Edition** 

  1. Does not work on Windows 10 build 2004 and upwards. You'll need to downgrade in order to play.
  (**To check your windows version: winkey + r then type winver**)
  - **If you have the version required, do the following:**
  1. Go to 'Developer Settings'
  2. Enable Developer Mode.
  3. Once Developer mode is enabled, run the install.bat file in the folder.
  4. Once that's done, search your pc for minecraft and run the exe as admin.

**Black Ops 2** 

   1. Piry (for Black Ops 2) is OFFLINE so you will have to get plutonium on your own at https://plutonium.pw/


**Sekiro** Character not moving 

   1. Sekiro has to be extracted differently then other games, go back and disable your antivirus and windows defender and  extract by clicking "Extract Here" 
   2. Go to in game settings - Input Device Options - Set the input device to Keyboard and Mouse.

**Train Simulator 2019** Setup 

    1.) Go through the setup until it asks you to enter the serial key

    2.) Open the keygen, in the top bar enter your email (it can be a temporary email) and then press continue. It'll create  a serial key in the second line which you can paste into the setup. It'll let you continue the setup after that

**The Forest**

    1.Download these files https://uploadhub.co/fb7ac561ce2f4219775e14ec22947f90/AGFY-The.Forest.Multiplayer.Fix.rar and extract and follow the instructions 

**Subnautica: Below Zero** Steam error 

    1.Make sure steam is running
    2.Then in the same folder as the exe is in, drag steam_api64.dll onto the desktop
    3.Right click the exe file and run as admin

**Subnautica: Below Zero** Not loading in properly 
   
    1.) Press F3 to open a sub-menu which will appear in the upper-left hand corner of the screen.
    2.) Press F8 to free the mouse.
    3.) Uncheck the 'Disable Console' option.
    4.) Back out by pressing F3 and F8 again. 
    5.) You should now be able to open the console with the tilde (~) key. It will appear as a grey box in the lower-left corner.

type inside the console "biome safe" and you should be teleported to the start of the game

**Astroneer**
    
    1.Extract the rar and copy the contents of the folder that you extract into the main games folder, hit replace if needed - https://uploadhub.to/ee640f5998bdf36526757c529eb73e7e/AGFY-ASTRONEER_Fix_MP.rar

**Monster Hunter World**
    
    1.Extract the rar and copy the contents of the folder that you extract into the main games folder, hit replace if needed 
    -https://uploadhub.co/8f8aadbe0f0c861d2f0ab8b3a71d683d/AGFY-MONSTER-HUNTER-WORLD-FIX.rar

**a way out: fixing steam initialization error:**

    1.Open Steam;
    2.Press Windows + R;
    3.In opened window put  steam://install/349620 and press OK;
    4.Start BlastZone 2 Demo installation for a few second, then stop and delete it;
    5.Launch the game.


**Blade and Sorcery opening steam page prompting to buy**

    Delete "BladeAndSorcery" then rename "BladeAndSorcery.exe.unpacked" to "BladeAndSorcery"


**Grand Theft Auto 4:GTA IV: fatal error: unrecoverable error: please restart the game (fix files)**

  download the files from this link and extract. Then follow install.txt https://uploadhub.to/fb7d15c18b3c47a3ef59b43ba7062cc1/AGFY-GTA_IV_FATAL.rar

**Overcooked 2 multiplayer fix**
    1.Extract the rar and copy the contents of the folder that you extract into the main games folder, hit replace if needed - https://uploadhub.to/4cdf623e8d75fa24b138e196ffd8d656/AGFY-O2_Fix_Repair.rar

**The Walking Dead Saints & Sinners**

    1.TO MAKE THIS WORK, MAKE A DESKTOP SHORTCUT FOR TWD FILE, THEN RIGHT CLICK, GO TO PROPERTIES, GO TO SHORTCUT AND IN THE TARGET SECTION, PUT A SPACE AND ADD "-steam" It should be like this: normalstuffherelikedriveandfolder\The Walking Dead Saints & Sinners\TWDSaintsAndSinners\TWD.exe" -steam

**satisfactory multiplayer instructions** launches epic games what to do:

    for satisfactory to work the foloowing is needed if any one asks

    1) Launch the Epic Games Store , go to your profile.
    2) Add Hello Neighbor Mod Kit to your library (don't download it);
    3) Launch the game through the launcher.

    and lastly to run game do this:
    Run the game through Launcher.exe In the game: Connection: Join Game
    Create a server:
    New Game --> Set up everything as you wish (set Session Type to Friends Only ) and click Start Game-->We are waiting for other players to connect.


**Cyberpunk2077** Language Change

1. You need to edit the "goggame-1423049311.info" file in the Cyberpunk 2077 folder, and change it from Russian to English and ru-RU to en-US
2. Open it with Notepad

**Assassins Creed:Valhalla** Language Packs

- https://uploadhub.to/folder/378/acv_language_packs


**Forza Horizon 4** Connecting a controller

- https://cdn.discordapp.com/attachments/758077808852598887/823974464584548444/how_to_connect_a_controller_to_steam_if_its_showing_you_are_playing_spacewar.pdf

**Insurgency Sandstorm** Opening steam

- https://cdn.discordapp.com/attachments/830166059972558888/834499816070447144/unknown.png

**The program was unable to start corretly. Error code 0x000000142**

Open your cmd as **admin** and run these two commands seperately (copy paste them)
- for %i in (%windir%\system32*.dll) do regvr32.exe / s%i
- for %i in (%windir%\system32*.ocx) do regsvr32.exe / s%i

Please **restart your pc** once this is done!!

*All these commands do, is restore any missing dll's from your pc.*

**Assassin's Creed Revelations** fix for glitches
   
    Go to the file directory where the game.exe is, find the systemdetection.dll file.
    Rename this file to systemdetection_0.dll 
    Relaunch your game.


##### Softwares 


 

**Microsoft Office** Setup 

 1. Extract the zip/rar file with antivirus closed *make sure internet connection is
on but switch off antivirus*.
 2. Go in the extracted folder, go to the ISO folder, right click on the 'Microsoft
Office Professional Plus 2016-2019 Retail-VL Version 2005.ISO' and do 'Extract
Here' (you need Winrar).
 3. Run OInstall.exe, choose the Language you prefer and the products that you want,
and you can change Microsoft Office 2016 to 2019/2013, 2019 is preferable because
it's the latest. You can choose between x86 and x64, x64 is preferable. It is
recommended to untick:
- Skype for business
- Onedrive for business
- Onedrive
- Outlook
- OneNote (I think this one is free already on the Windows Store)
If you're not going to use them, untick them (Onedrive is annoying lol). I
installed the 2019 version, let me know how it goes with 2013/2016 in the comments.
 4. Now click 'Install Office'.
 5. After the installing is finished, it should open back the OInstall software. Go
to the Utilities tab that's next to the Main Windows tab, and make sure KMS online
is ticked, then click on the 'Office RETAIL => VL' button, and wait till it
finishes converting the Office. It should take some time.
 6. After some amount of time, it should say '******Completed******'.
 7. After step 5 and 6 now click on "activate office"
 8. Close everything and Enjoy the Office softwares!
NOTE: AFTER AROUND 3 MONTHS U MAY HAVE TO REACTIVATE OFFICE SO PLS FOLLOW STEP 5 TO
7 AGAIN BUT MAKE SURE UNDER UTILITES MAKE SURE YOU HAVE SELECTED/TICKED REACTIVATE

**FACERIG**

    
    This works for 64bit users
    This is how I got it to work
    Copy the crack to the installation path except the Launcer.exe file

    Just copy this in a text file and edit it as required
    1) replace <installation path=""> with the path you extracted it at
    2) replace <user name=""> with you User Name
    then save it as Facerig.reg and open it, click yes, click ok

    Windows Registry Editor Version 5.00

    [HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Holotech]

    [HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Holotech\FaceRig]
    "path"="<installation path="">"
    "Exe_Path"="<installation path="">\Bin\FaceRig.exe"
    "BcstSplashPath"="<installation path="">\Mod\VP\PC\OfflineScreens"
    "broadcast"="OFF"
    "ParentalContentType"="Mature"
    "ParentalFirstTime"="1"
    "UserPath"="C:\Users<user name="">\Documents\Holotech\FaceRig"


**Wallpaper Engine Steam Workshop**
1. First find your desired wallpaper from: https://steamcommunity.com/workshop/about/?appid=431960
2. Copy the link for the wallpaper you want and paste it into here, this website will allow you to download it: https://steamworkshopdownloader.io/
3. Extract the rar file
4. Open wallpaper engine and on the bottom left choose "open from file". Select scene.pkg from the extracted file (if that doesn't work then choose project.json)
- **If you get a blackscreen when selecting a wallpaper, do the following:**
https://cdn.discordapp.com/attachments/758077802804412431/829726540954337290/wall.gif


